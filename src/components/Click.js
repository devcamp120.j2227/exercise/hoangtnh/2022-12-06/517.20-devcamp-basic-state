import { Component } from "react";

class Click extends Component {
    constructor(props){
        super(props);
        this.state ={
            count: 0
        }
        //C1: sử dụng bind để trỏ this về class
        //hàm bind có tác dụng trỏ lại this cho 1 hàm
        //this.onBtnClickHandle = this.onBtnClickHandle.bind(this);
    }
    //C2: thường được dùng hiện tại
    //arrow function không có this nên this trong arrow function mặc định trỏ về class
    onBtnClickHandle = () =>{
        console.log("Clicked");
        this.setState({
            count: this.state.count + 1
        })
    }
    render(){
        return(
            <div>
                <p>Count Click: {this.state.count}</p>
                <hr/>
                <button onClick={this.onBtnClickHandle}>Click here</button>
            </div>
        )  
    }
}
export default Click;